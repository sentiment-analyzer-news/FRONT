export const initialState = {
    results: [],
    loading: false
};

export const SearchReducer = (initialState:any, action:any) => {
    switch(action.type) {
        case 'SEARCH_SUCCESS':
            const results = action.payload
            return {
                ...initialState, 
                results,
                loading: false
            };
        case 'SEARCH_REQUEST':
            return {
                ...initialState, 
                loading: true
            }
        case 'SEARCH_ERROR':
            return {
                ...initialState, 
                loading: false
            }
        default:
            throw new Error('Unhandled action type:' + action.type)
    }
}
