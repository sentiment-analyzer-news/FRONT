import {
  createContext,
  useContext,
  useReducer,
  ReactNode,
} from "react";
import { initialState, SearchReducer } from "./SearchReducer";


type SearchDispatch = React.Dispatch<SearchAction>;

type SearchAction = | { type: 'SEARCH_REQUEST'}
| { type: 'SEARCH_SUCCESS'; payload: SearchResult[] }
| { type: 'SEARCH_ERROR'; };

type SearchResult = {
    title: any
}

const SearchContext = createContext<any>([]);
const SearchDispatchContext = createContext<SearchDispatch | null>(null);

export function SearchProvider({ children }: { children: ReactNode }) {
  const [results, dispatch] = useReducer(SearchReducer, initialState);
  
  return (
    <SearchContext.Provider value={results}>
      <SearchDispatchContext.Provider value={dispatch}>
        {children}
      </SearchDispatchContext.Provider>
    </SearchContext.Provider>
  );
}

export function useSearch() {
  const context = useContext(SearchContext);

  if (context === undefined) {
    throw new Error("useSearch must be used within a AuthProvider");
  }

  return context;
}

export function useSearchDispatch() {
  const context = useContext(SearchDispatchContext);
  if (context === undefined) {
    throw new Error("useSearchDispatch must be used within a AuthProvider");
  }

  return context;
}
