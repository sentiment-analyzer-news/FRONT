import React from "react";

interface ColorGradientProps {
  score: number;
  magnitude: number;
}

const ScoreIndex: React.FC<ColorGradientProps> = ({ score, magnitude }) => {
  const getColor = (value: number): string => {
    const normalizedValue = (value + 1) / 2; // Normalize the value between 0 and 1
    const red = Math.round(255 * (1 - normalizedValue));
    const green = Math.round(255 * normalizedValue);
    const blue = 0;

    return `rgb(${red}, ${green}, ${blue})`;
  };

  const color = getColor(score);

  return (
    <div className="text-sm text-gray-900 flex items-center gap-4">
      <div
        title={score + " / " + magnitude}
        className="w-3 h-3 rounded-full"
        style={{
            backgroundColor: color,
            boxShadow: `0 0 5px ${color}`,
        }}
        ></div>
        {score} / {magnitude}
    </div>
  );
};

export default ScoreIndex;
