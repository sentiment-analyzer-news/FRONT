import ContentLoader from 'react-content-loader'

const HeadBodyGrid = ({ ...rest }) => (
    <ContentLoader height="500" width="1000" viewBox="0 0 265 230" {...rest}>
      <rect x="0" y="0" rx="4" ry="4" width="3500" height="25" />
      
    </ContentLoader>
  )
  
  HeadBodyGrid.metadata = {
    name: 'Didier Munezero',
    github: 'didiermunezero',
    description: 'Grid for content of head and body',
    filename: 'HeadBodyGrid',
  }
  
  export default HeadBodyGrid