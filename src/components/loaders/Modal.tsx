import { useSearch } from "../../context/SearchContext";

function Modal() {
  const { loading } = useSearch();
 
  return (
    <div>
      

      {loading && (
        <div className="fixed inset-0 flex items-center justify-center z-50">
          <div className="absolute inset-0 bg-gray-900 opacity-50"></div>
          <div className="bg-white rounded-lg p-8 z-10">
            {/* Modal Content */}
            <div className="flex items-center mb-4">
              <svg
                className="animate-spin h-5 w-5 mr-2 text-blue-500"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
              >
                <circle
                  className="opacity-25"
                  cx="12"
                  cy="12"
                  r="10"
                  stroke="currentColor"
                  strokeWidth="4"
                ></circle>
                <path
                  className="opacity-75"
                  fill="currentColor"
                  d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647zM12 20c3.042 0 5.824-1.135 7.938-3l-2.647-3A7.962 7.962 0 0112 16v4zm5.291-6A7.963 7.963 0 0112 20v4c4.418 0 8-3.582 8-8h-4zm-9.938-3A7.963 7.963 0 0112 4V0C7.582 0 4 3.582 4 8h4zm-1.353 7.938l-3 2.647A7.962 7.962 0 010 12h4c0 4.418 3.582 8 8 8v-4a7.963 7.963 0 01-3.647-1.062zM16 12a4 4 0 11-8 0 4 4 0 018 0z"
                ></path>
              </svg>
              <span>Search in progress...</span>
            </div>
            {/* End of Modal Content */}


          </div>
        </div>
      )}
    </div>
  );
}

export default Modal;
