import { useEffect } from "react";
import { useSearch } from "../context/SearchContext";
import ScoreIndex from "./ScoreIndex";
import Modal from "./loaders/Modal";
import { FaExternalLinkAlt } from 'react-icons/fa';


export default function TableResults() {
  const { results } = useSearch();
 

  useEffect(() => {
    console.log(results);
  }, [results]);

  
  return (
    <div className="overflow-x-auto">
      <Modal />
      {Array.isArray(results) && results.length > 0 ? (
        <table className="table-auto min-w-full divide-y divide-gray-200">
          <thead>
            <tr>
              <th className="px-6 py-3 bg-gray-100 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Head Title
              </th>
              <th className="px-6 py-3 bg-gray-100 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Score / Magnitude
              </th>
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-200">
            {Array.isArray(results) &&
              results.map((result: any) => (
                <tr>
                  <td className="px-6 py-4 whitespace-normal">
                    <div className="text-sm text-gray-900 link-container">
                    <a href={result.title.link} className="hover:text-blue-500 focus:text-blue-500 flex items-center link" target="_blank" rel="noopener noreferrer">
                      <span className="underline">{result.title.text}</span>
                      <span className="ml-1"><FaExternalLinkAlt /></span>
                  </a>
                    </div>
                  </td>
                  <td className="px-6 py-4 whitespace-normal">
                      <ScoreIndex score={result.score} magnitude={result.magnitude}/>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      ) : (
        <div className="flex items-center justify-center h-full">
          <div>No results found.</div>
        </div>
      )}
    </div>
  );
}
