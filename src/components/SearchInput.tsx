import { useState } from "react";
import axios from "axios";
import { useSearchDispatch } from "../context/SearchContext";

export default function SearchInput() {
  const [searchQuery, setSearchQuery] = useState("");
  const [hasError, setHasError] = useState(false);
  const dispatch = useSearchDispatch();

  const handleSearch = async () => {
    if (searchQuery === "") {
      setHasError(true);
      return;
    }

    if (dispatch) {
      dispatch({ type: "SEARCH_REQUEST" });
    }

    try {
      const response = await axios.get(
        `${process.env.REACT_APP_API_BASE_URL}/analyze_sentiment/?source=googlenews&keyword=${searchQuery}`
      );

      // Check if response.data is not empty and has a response array and response array is not empty
      if (
        response.data &&
        response.data.response &&
        response.data.response.length > 0
      ) {

        if (dispatch) {
          dispatch({ type: "SEARCH_SUCCESS", payload: response.data.response });
        }
      }
    } catch (error) {
      if (dispatch) {
        dispatch({ type: "SEARCH_ERROR" });
      }
      console.error("Error:", error);
    }
  };

  return (
    <div className="flex flex-col md:flex-row md:justify-center gap-3">
      <div className="relative">
        <input
          type="text"
          className="w-full py-2 px-4 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500 md:w-96"
          placeholder="S&P500"
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
        />
        {hasError && (
          <div className="text-red-500 text-xs italic absolute  top-12 left-0 bottom-0">
            Le champs est requis
          </div>
        )}
      </div>
      <button
        onClick={handleSearch}
        className="px-4 py-2 bg-blue-500 text-white rounded-md shadow hover:bg-blue-600 focus:outline-none focus:ring-blue-500 focus:ring-2"
      >
        Search
      </button>
    </div>
  );
}
