export default function ErrorMessageInput() {
    return (
        <div>
            <p className="text-red-500 text-xs italic">Le champs est requis</p>
        </div>
    )
}