import "./App.css";
import Search from "./views/Search";
import { SearchProvider } from './context/SearchContext';

function App() {
  return  (
  
  <div className="h-screen bg-gray-100 py-10 px-10">
    <SearchProvider>
      <Search />
    </SearchProvider>
  </div>
  );
}

export default App;
