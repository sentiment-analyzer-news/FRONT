import SearchInput from "../components/SearchInput";
import TableResults from "../components/TableResults";

export default function Search() {
  return (
    <div className="flex flex-col gap-10">
        <SearchInput />
        <TableResults/>
    </div>
  );
}
